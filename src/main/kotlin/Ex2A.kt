import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val dies = listOf<String>("Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte", "Diumenge")
    // Llista de dies, que conté llista de temperatures
    val temperatures: MutableList<MutableList<Double>> = mutableListOf()
    // 21 32 22 33 23 34 24 35 25 36 26 37 27 38
    var minima: Double
    var maxima: Double
    for (i in 0 until 7) {
        // Llista de temperatures d'un dia
        val temperatureDay = mutableListOf<Double>()
        // Temperatura mínima
        temperatureDay.add(scanner.nextDouble())
        // Temperatura màxima
        temperatureDay.add(scanner.nextDouble())
        temperatures.add(temperatureDay)
    }
    var diaMinima: String = ""
    var diaMaxima: String = ""
    minima = temperatures[0][0]
    maxima = temperatures[0][0]
    for (i in 0 until 7) {
        println("Temperatura mitjana de ${dies[i]} és ${(temperatures[i][0]+temperatures[i][1])/2}")
        if (temperatures[i][0] <= minima) {
            minima = temperatures[i][0]
            diaMinima = dies[i]
        }
        if (temperatures[i][1] >= maxima) {
            maxima = temperatures[i][1]
            diaMaxima = dies[i]
        }
    }
    println("Temperatura minima va ser $diaMinima de ${minima + 273.15}")
    println("Temperatura maxima va ser $diaMaxima de ${(maxima * 9 / 5) + 32}")
}