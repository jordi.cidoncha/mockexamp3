import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    var verb: String

    while (true) {
        verb = scanner.next()
        // mirar si es verb infinitiu i quina conjugació
        when (verb.takeLast(2)) {
            "ar" -> println("El verb $verb és de la primera conjugació")
            "er", "re" -> println("El verb $verb és de la segona conjugació")
            "ir" -> println("El verb $verb és de la tercera conjugació")
            else -> println("ERROR")
        }
    }
}