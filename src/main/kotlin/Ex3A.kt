import java.util.*

fun main() {
    val scanner = Scanner(System.`in`)
    val n = scanner.nextInt()
    val m = scanner.nextInt()
    val matrixOfNumbers: MutableList<MutableList<Int>> = MutableList(n) {MutableList(m) {0}}

    for (i in 0 until n) {
        matrixOfNumbers[i][0] = 1
        matrixOfNumbers[i][m-1] = 1
    }
    for (i in 0 until m) {
        matrixOfNumbers[0][i] = 1
        matrixOfNumbers[n-1][i] = 1
    }
    for (i in 0 until n) {
        for (j in 0 until m) {
            print("${matrixOfNumbers[i][j]} ")
        }
        println()
    }

}