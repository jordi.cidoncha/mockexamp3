import java.util.Scanner

fun main() {
    val scanner = Scanner(System.`in`)

    while (true) {
        val mida = scanner.nextInt()
        val sequence = mutableListOf<Int>()
        for (i in 0 until mida) {
            sequence.add(scanner.nextInt())
        }
        if (mida%2 == 1) sequence[mida/2] *= -1
        else {
            sequence[mida/2] *= -1
            sequence[(mida/2)-1] *= -1
        }
        println(sequence)
    }
}