import java.util.*

fun main() {
    val textCodificat = """BI XJLO, ZLJL ZFBDL NRB BP, FJMFAB X ILP XJXKQBP SBO IXP AFSBOQFAXP QLKQBOÍXP NRB ZLJBQBK.
        COBZRBKQX RK XÑL JÁP JF ZLJMXÑÍX, V BI BZL AB QR SLW QB PBOÁ BUQOXÑL.
        BI JFPJL AFXYIL ZFQXOÁ IXP PXDOXAXP BPZOFQROXP PF SFBKB YFBK X PRP MOLMÓPFQLP.
        VL QLJL BI JRKAL MLO IL NRB BP, RK QBXQOL BK BI NRB ZXAX ELJYOB OBMOBPBKQX PR MXOQB.
        IX ZXYBWX MLAOÁ AFZQXO IBVBP ZLKQOX IX MXPFÓK, MBOL BI XOALO
        MRBAB JÁP NRB IX COFXIAXA AB RKX PBKQBKZFX.
        IBB BI ABZOBQL QÚ JFPJL: VX NRB MFABP GRPQFZFX, QBK MLO ZFBOQL NRB QBKAOÁP JÁP GRPQFZFX AB IX NRB ABPBXP.
        BI AFXYIL ZFQX IX YFYIFX BK PR MOLSBZEL."""

    for (lletra in textCodificat) {
        if (lletra in 'A'..'Z')
            print(if (lletra.code + 3 > 90) Char((lletra.code + 3) % 10 + 64) else Char(lletra.code + 3))
        else print(lletra)
    }

}

