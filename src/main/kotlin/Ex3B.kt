import java.lang.Math.abs
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.util.Scanner

fun main () {
    val scanner = Scanner(System.`in`)
    println("Introduce name of first person:")
    val name1 = scanner.next()
    println("Introduce date of birth of first person:")
    val date1 = scanner.next()
    println("Introduce name of second person:")
    val name2 = scanner.next()
    println("Introduce date of birth of second person:")
    val date2 = scanner.next()

    val yearOfBirth1 = date1.split("/")[2].toInt()
    val monthOfBirth1 = date1.split("/")[1].toInt()
    val dayOfBirth1 = date1.split("/")[0].toInt()
    val yearOfBirth2 = date2.split("/")[2].toInt()
    val monthOfBirth2 = date2.split("/")[1].toInt()
    val dayOfBirth2 = date2.split("/")[0].toInt()

    val dateOfBirth1 = LocalDate.of(yearOfBirth1, monthOfBirth1, dayOfBirth1)
    val dateOfBirth2 = LocalDate.of(yearOfBirth2, monthOfBirth2, dayOfBirth2)
    val diffInDays = abs(ChronoUnit.DAYS.between(dateOfBirth1, dateOfBirth2))
    println("${diffInDays/365} anys ${diffInDays%365/30} mesos ${diffInDays%365%30} dies")
    if (dateOfBirth1<dateOfBirth2) println("$name1 és més gran que $name2")
    else println("$name2 és més gran que $name1")
}
//4/6/1988
//26/7/2003